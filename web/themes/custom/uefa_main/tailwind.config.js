/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './templates/**/*.html.twig',
    './components/**/*.twig',
    './components/**/*.stories.json',
    './components/**/*.stories.yml',
    './*.theme',
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
